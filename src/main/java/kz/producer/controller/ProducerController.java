package kz.producer.controller;

import kz.producer.entity.Producer;
import kz.producer.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/producer")
public class ProducerController {
    @Autowired
    private ProducerService producerService;

    @PostMapping
    public Producer createProducer(@RequestBody Producer producer) {
        return producerService.createProducer(producer.getStart(), producer.getDifference());
    }

    @GetMapping("/{id}")
    public Producer getProducer(@PathVariable Long id) {
        return producerService.getProducer(id);
    }

    @PutMapping("/{id}")
    public Producer updateProducer(@PathVariable Long id, @RequestBody Producer producer) {
        return producerService.updateProducer(id, producer.getStart(), producer.getDifference());
    }
}
