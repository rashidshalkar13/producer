package kz.producer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Stack;

@Component
public class Printer {
    @Autowired
    private Stack<Integer> outputStack;

    private boolean isPrintingWindow = false;

    public void printOutput() {
        if (isPrintingWindow) {
            if (!outputStack.isEmpty()) {
                System.out.println(outputStack.pop());
            }
        }
    }

    @Scheduled(fixedRate = 10000) // 10 seconds
    public void togglePrintingWindow() {
        isPrintingWindow = !isPrintingWindow;
    }
}
