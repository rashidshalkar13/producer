package kz.producer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArithmeticProgression {

    private static Stack<Integer> outputStack = new Stack<>();
    private static Random random = new Random();
    private static boolean isPrintingWindow = false;

    private static int generateArithmeticProgression(int start, int difference, int n) {
        return start + (n - 1) * difference;
    }
    private static void printOutput() {
        if (isPrintingWindow) {
            if (!outputStack.isEmpty()) {
                System.out.println(outputStack.pop());
            }
        }
    }

    private static class Producer implements Runnable {

        private int start;
        private int difference;
        private int counter;

        public Producer(int start, int difference) {
            this.start = start;
            this.difference = difference;
            this.counter = 1;
        }

        @Override
        public void run() {
            while (true) {
                int output = generateArithmeticProgression(start, difference, counter);

                outputStack.push(output);
                counter++;

                int interval = random.nextInt(5) + 1;

                try {
                    Thread.sleep(interval * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class Printer implements Runnable {

        @Override
        public void run() {
            while (true) {
                printOutput();

                int interval = random.nextInt(5) + 1;

                try {
                    Thread.sleep(interval * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                isPrintingWindow = !isPrintingWindow;
            }
        }
    }

//    @PostConstruct
    public void call() {
        Producer producer1 = new Producer(3, 3); // {3, 6, 9, ...}
        Producer producer2 = new Producer(5, 5); // {5, 10, 15, ...}

        Printer printer = new Printer();

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        executorService.submit(producer1);
        executorService.submit(producer2);
        executorService.submit(printer);
    }
}
