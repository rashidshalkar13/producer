package kz.producer.service;

import kz.producer.entity.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Stack;

@Component
public class Scheduler {
    @Autowired
    private ProducerService producerService;
    @Autowired
    private Stack<Integer> outputStack;
    @Autowired
    private Printer printer;

    @Scheduled(fixedRate = 5000) // 5 seconds
    public void generateOutput() {
        List<Producer> producers = producerService.getAllProducers();
        for (Producer producer : producers) {
            int output = producerService.generateArithmeticProgression(producer);
            outputStack.push(output);
            printer.printOutput();
        }
    }
}
