package kz.producer.service;

import kz.producer.entity.Producer;
import kz.producer.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProducerService {
    @Autowired
    private ProducerRepository producerRepository;

    public Producer createProducer(int start, int difference) {
        Producer producer = new Producer();
        producer.setStart(start);
        producer.setDifference(difference);
        producer.setCounter(1);
        return producerRepository.save(producer);
    }

    public Producer getProducer(Long id) {
        return producerRepository.findById(id).orElse(null);
    }

    public Producer updateProducer(Long id, int start, int difference) {
        Producer producer = producerRepository.findById(id).orElse(null);
        if (producer != null) {
            producer.setStart(start);
            producer.setDifference(difference);
            producer.setCounter(1);
            return producerRepository.save(producer);
        }
        return null;
    }

    public int generateArithmeticProgression(Producer producer) {
        int output = producer.getStart() + (producer.getCounter() - 1) * producer.getDifference();
        producer.setCounter(producer.getCounter() + 1);
        producerRepository.save(producer);
        return output;
    }

    public List<Producer> getAllProducers() {
        return producerRepository.findAll();
    }
}