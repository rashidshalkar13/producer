CREATE TABLE if NOT EXISTS producer
(
    id                          serial NOT NULL,
    start                       INTEGER,
    difference                  INTEGER,
    counter                     INTEGER,
    constraint producer_pk
    primary key (id)
);