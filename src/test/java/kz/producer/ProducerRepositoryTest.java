package kz.producer;

import kz.producer.entity.Producer;
import kz.producer.repository.ProducerRepository;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProducerRepositoryTest {

    @Autowired
    private ProducerRepository producerRepository;

    @Test
    void testSave() {
        Producer producer = new Producer();
        producer.setStart(3);
        producer.setDifference(3);
        producer.setCounter(1);

        Producer savedProducer = producerRepository.save(producer);

        assertNotNull(savedProducer.getId());

        assertEquals(producer.getStart(), savedProducer.getStart());
        assertEquals(producer.getDifference(), savedProducer.getDifference());
        assertEquals(producer.getCounter(), savedProducer.getCounter());
    }

    @Test
    void testFindById() {
        Producer producer = new Producer();
        producer.setStart(5);
        producer.setDifference(5);
        producer.setCounter(1);

        Producer savedProducer = producerRepository.save(producer);

        Producer foundProducer = producerRepository.findById(savedProducer.getId()).orElse(null);

        assertNotNull(foundProducer);

        assertEquals(savedProducer.getId(), foundProducer.getId());
        assertEquals(savedProducer.getStart(), foundProducer.getStart());
        assertEquals(savedProducer.getDifference(), foundProducer.getDifference());
        assertEquals(savedProducer.getCounter(), foundProducer.getCounter());
    }

    @Test
    void testFindAll() {
        Producer producer1 = new Producer();
        producer1.setStart(3);
        producer1.setDifference(3);
        producer1.setCounter(1);

        Producer producer2 = new Producer();
        producer2.setStart(5);
        producer2.setDifference(5);
        producer2.setCounter(1);

        producerRepository.save(producer1);
        producerRepository.save(producer2);

        List<Producer> producers = producerRepository.findAll();

        assertFalse(producers.isEmpty());

        assertTrue(producers.contains(producer1));
        assertTrue(producers.contains(producer2));
    }
}
