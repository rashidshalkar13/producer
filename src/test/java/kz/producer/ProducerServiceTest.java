package kz.producer;

import kz.producer.entity.Producer;
import kz.producer.repository.ProducerRepository;
import kz.producer.service.ProducerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProducerServiceTest {

    @Mock
    private ProducerRepository producerRepository;

    @InjectMocks
    private ProducerService producerService;

    @Test
    void testCreateProducer() {
        Producer producer = new Producer();
        producer.setStart(3);
        producer.setDifference(3);
        producer.setCounter(1);

        when(producerRepository.save(producer)).thenReturn(producer);

        Producer createdProducer = producerService.createProducer(3, 3);

        assertNotNull(createdProducer);

        assertEquals(producer.getStart(), createdProducer.getStart());
        assertEquals(producer.getDifference(), createdProducer.getDifference());
        assertEquals(producer.getCounter(), createdProducer.getCounter());
    }

    @Test
    void testGetProducer() {
        Producer producer = new Producer();
        producer.setStart(5);
        producer.setDifference(5);
        producer.setCounter(1);

        when(producerRepository.findById(1L)).thenReturn(Optional.of(producer));

        Producer foundProducer = producerService.getProducer(1L);

        assertNotNull(foundProducer);

        assertEquals(producer.getStart(), foundProducer.getStart());
        assertEquals(producer.getDifference(), foundProducer.getDifference());
        assertEquals(producer.getCounter(), foundProducer.getCounter());
    }

    @Test
    void testUpdateProducer() {
        Producer producer = new Producer();
        producer.setStart(3);
        producer.setDifference(3);
        producer.setCounter(1);

        when(producerRepository.findById(1L)).thenReturn(Optional.of(producer));

        when(producerRepository.save(producer)).thenReturn(producer);

        Producer updatedProducer = producerService.updateProducer(1L, 5, 5);

        assertNotNull(updatedProducer);

        assertEquals(5, updatedProducer.getStart());
        assertEquals(5, updatedProducer.getDifference());
        assertEquals(1, updatedProducer.getCounter());
    }

    @Test
    void testGenerateArithmeticProgression() {
        Producer producer = new Producer();
        producer.setStart(3);
        producer.setDifference(3);
        producer.setCounter(1);

        when(producerRepository.save(producer)).thenReturn(producer);

        int output = producerService.generateArithmeticProgression(producer);

        assertEquals(3, output);

        assertEquals(2, producer.getCounter());
    }
}

